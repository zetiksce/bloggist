<?php

namespace Bloggist\Bundle\PostRssBundle\Controller;

use Bloggist\Component\Rss\FeedBuilderInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of FeedController
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class FeedController
{
    /**
     * @var \Bloggist\Component\Rss\FeedBuilderInterface
     */
    private $feedBuilder;

    public function __construct(FeedBuilderInterface $feedBuilder)
    {
        $this->feedBuilder = $feedBuilder;
    }

    public function show()
    {
        $feed = $this->feedBuilder->build();

        $headers = array(
            'Content-Type' => 'application/rss+xml',
            // @todo Add some more useful headers
        );

        return new Response($feed->render(), 200, $headers);
    }

}