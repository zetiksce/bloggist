<?php

namespace Bloggist\Bundle\PostDoctrineBundle\Service;

use Bloggist\Component\Filter\FilterInterface;
use Bloggist\Component\Service\PageInterface;
use Bloggist\Component\Service\ServiceInterface;
use Doctrine\ORM\EntityManager;

/**
 * Facade for post repository -- includes validation.
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class PostService implements ServiceInterface
{
    /**
     * @var PostRepositoryInterface
     */
    private $repository;
    
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(PostRepositoryInterface $repository, EntityManager $entityManager)
    {
        $this->repository = $repository;
        $this->entityManager = $entityManager;
    }
    
    public function find($identifier)
    {
        return $this->repository->find($identifier);
    }
    
    public function store(/* object */ $post)
    {
        $this->entityManager->persist($post);
        $this->entityManager->flush();
    }

    public function index(FilterInterface $filter, $sort, $order)
    {
        // @todo: validate parameters
        return $this->repository->index($filter, $sort, $order);
    }

    public function paginate(PageInterface $page, FilterInterface $filter, $sort, $order)
    {
        // @todo: validate parameters
        return $this->repository->paginate($page, $filter, $sort, $order);
    }

    public function findOneBySlug($slug)
    {
        // @todo: validation
        return $this->repository->findOneBySlug($slug);
    }
}