<?php

namespace Bloggist\Bundle\PostDoctrineBundle\Entity\Repository;

use Bloggist\Bundle\PostDoctrineBundle\Service\PostRepositoryInterface;
use Bloggist\Component\Filter\PostFilterInterface;
use Bloggist\Component\Service\PageInterface;
use Bloggist\Component\Service\ResultPage;

use Doctrine\ORM\EntityRepository;

/**
 * Description of PostRepository
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class PostRepository extends EntityRepository implements PostRepositoryInterface
{
    
    public function find($id)
    {
        return parent::find($id);
    }

    public function index(PostFilterInterface $filter, $sort, $order)
    {
        return $this->buildQueryBuilder($filter, $sort, $order)->getQuery()->getResult();
    }

    public function paginate(PageInterface $page, $filter, $sort, $order)
    {
        $qb = $this->buildQueryBuilder($filter, $sort, $order);

        $countQb = clone $qb;
        $itemsCount = $countQb->select('count(p)')->getQuery()->getResult();

        $qb->setMaxResults($page->getSize());
        $qb->setFirstResult(($page->getIndex() - 1) * $page->getSize());

        return new ResultPage($page, $qb->getQuery()->getResult(), $itemsCount);
    }

    public function buildQueryBuilder(PostFilterInterface $filter, $sort, $order)
    {
        $qb = $this->createQueryBuilder('p');

        if ($filter->hasStatuses()) {
            $qb->andWhere($qb->expr()->in('p.status', $filter->getStatuses()));
        }

        if ($filter->hasPostedBefore()) {
            $qb->andWhere($qb->expr()->lt('p.postedAt', ':posted_before'))->setParameter('posted_before', $filter->getPostedBefore());
        }

        if ($filter->hasPostedAfter()) {
            $qb->andWhere($qb->expr()->gt('p.postedAt', ':posted_after'))->setParameter('posted_after', $filter->getPostedAfter());
        }

        $qb->orderBy('p.' . $sort, $order);

        return $qb;
    }

}