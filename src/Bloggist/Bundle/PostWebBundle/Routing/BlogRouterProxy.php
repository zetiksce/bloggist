<?php

namespace Bloggist\Bundle\PostWebBundle\Routing;

use Bloggist\Component\Entity\Blog;
use Bloggist\Component\Routing\RouterProxyInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Description of BlogRouterProxy
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class BlogRouterProxy implements RouterProxyInterface
{
    /**
     * @var RouterInterface
     */
    private $router;
    private $routeName;

    public function __construct(RouterInterface $router, $routeName)
    {
        $this->router = $router;
        $this->routeName = $routeName;
    }

    public function generate($object, $absolute = false)
    {
        if (!$object instanceof Blog) {
            throw new InvalidTypeException('Bloggist\Component\Entity\Blog', $object);
        }

        /* @var $object \Bloggist\Component\Entity\Blog */
        return $this->router->generate($this->routeName, array(), $absolute);
    }

    public function handles($object)
    {
        return $object instanceof Blog;
    }
    
}