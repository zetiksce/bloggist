<?php

namespace Bloggist\Bundle\PostWebBundle\Routing;

use Bloggist\Component\Entity\Post;
use Bloggist\Component\Routing\RouterProxyInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Description of PostRouterProxy
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class PostRouterProxy implements RouterProxyInterface
{
    /**
     * @var RouterInterface
     */
    private $router;
    private $routeName;

    public function __construct(RouterInterface $router, $routeName)
    {
        $this->router = $router;
        $this->routeName = $routeName;
    }

    public function generate($object, $absolute = false)
    {
        if (!$object instanceof Post) {
            throw new InvalidTypeException('Bloggist\Component\Entity\Post', $object);
        }

        /* @var $object \Bloggist\Component\Entity\Post */
        return $this->router->generate($this->routeName, array('slug' => $object->getSlug()), $absolute);
    }

    public function handles($object)
    {
        return $object instanceof Post;
    }
    
}