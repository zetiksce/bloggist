<?php

namespace Bloggist\Bundle\PostAdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Bloggist\Bundle\PostAdminBundle\Form\DataTransformer\StatusToStringTransformer;

/**
 * Description of PostType
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
abstract class PostType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $modelTransformer = new StatusToStringTransformer();
        
        $builder->add('title');
        $builder->add($builder->create('status', 'text')->addModelTransformer($modelTransformer));
        $builder->add('slug');
        $builder->add('synopsis');        
    }
    
    public function getName()
    {
        return 'post';
    }
}