<?php

namespace Bloggist\Component\Filter;

/**
 * Description of PostFilterInterface
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
interface PostFilterInterface extends FilterInterface
{

    public function getStatuses();

    public function hasStatuses();

    /**
     * @return \DateTime
     */
    public function getPostedBefore();

    public function hasPostedBefore();

    public function setPostedBefore(\DateTime $postedBefore);

    /**
     * @return \DateTime
     */
    public function getPostedAfter();

    public function hasPostedAfter();

}