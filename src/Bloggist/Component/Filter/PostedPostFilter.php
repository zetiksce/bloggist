<?php

namespace Bloggist\Component\Filter;

use Bloggist\Component\Entity\Status\StatusComplete;

/**
 * Description of PostedPostFilter
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class PostedPostFilter extends PostFilter
{

    public function __construct()
    {
        $this->setStatuses(array(new StatusComplete));
        $this->setPostedBefore(new \DateTime);
    }
    
}