<?php

namespace Bloggist\Component\Routing;

use Bloggist\Component\Exception\InvalidTypeException;

/**
 * Description of RouterProxyManager
 *
 * @author Zbigniew Czapran <zbigniewczapran@timeout.com>
 */
class RouterProxyManager implements RouterProxyInterface
{
    private $proxies = array();

    public function registerProxy(RouterProxyInterface $proxy)
    {
        $this->proxies[] = $proxy;
    }

    public function generate($object, $absolute = false)
    {
        foreach ($this->proxies as $proxy) {
            if ($proxy->handles($object)) {
                return $proxy->generate($object, $absolute);
            }
        }

        throw new InvalidTypeException('[multiple types]', $object);
    }

    public function handles($object)
    {
        foreach ($this->proxies as $proxy) {
            if ($proxy->handles($object)) {
                return true;
            }
        }

        return false;
    }
}