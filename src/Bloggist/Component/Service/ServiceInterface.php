<?php

namespace Bloggist\Component\Service;

use Bloggist\Component\Filter\FilterInterface;

/**
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
interface ServiceInterface
{
    
    /**
     * @param mixed $identifier
     */
    public function find($identifier);

    public function index(FilterInterface $filter, $sort, $order);

    /**
     * @param \Bloggist\Component\Service\PageInterface $page
     * @param \Bloggist\Component\Filter\FilterInterface $filter
     * @param string $orderBy
     *
     * @return ResultPageInterface
     */
    public function paginate(PageInterface $page, FilterInterface $filter, $sort, $order);

}