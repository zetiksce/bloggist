<?php

namespace Bloggist\Component\Service;

/**
 * Description of ResultPage
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class ResultPage extends Page implements ResultPageInterface
{

    /**
     * @var array
     */
    private $itemsOnPage;

    /**
     * @var int
     */
    private $itemsCount;

    public function __construct(PageInterface $page, array $itemsOnPage, $itemsCount)
    {
        parent::__construct($page->getIndex(), $page->getSize());
        $this->itemsOnPage = $itemsOnPage;
        $this->itemsCount = $itemsCount;
    }

    public function countItems()
    {
        return $this->itemsCount;
    }

    public function countItemsOnPage()
    {
        return \count($this->itemsOnPage);
    }

    public function countNumberOfPages()
    {
        return \ceil($this->countItems() / $this->getSize());
    }

    public function getItems()
    {
        return $this->itemsOnPage;
    }

}