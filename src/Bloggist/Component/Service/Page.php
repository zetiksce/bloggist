<?php

namespace Bloggist\Component\Service;

/**
 * Description of Page
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class Page implements PageInterface
{
    private $index;
    private $size;

    public function __construct($index, $size)
    {
        $this->index = $index;
        $this->size = $size;
    }

    public function getIndex()
    {
        return $this->index;
    }

    public function getSize()
    {
        return $this->size;
    }
}