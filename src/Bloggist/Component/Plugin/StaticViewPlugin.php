<?php

namespace Bloggist\Component\Plugin;

use Symfony\Component\Templating\EngineInterface;

/**
 * StaticPlugin
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class StaticViewPlugin implements RenderablePluginInterface
{

    /**
     * @var EngineInterface
     */
    private $templatingEngine;

    /**
     * @var string
     */
    private $template;

    public function __construct(
            EngineInterface $templating,
            $template)
    {
        $this->templatingEngine = $templating;        
        $this->template = $template;
    }
    
    public function render(/* object */ $subject, array $params = array())
    {
        return $this->templatingEngine->render($this->template, array('subject' => $subject, 'params' => $params));
    }

}