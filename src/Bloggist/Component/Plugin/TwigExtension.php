<?php

namespace Bloggist\Component\Plugin;

/**
 * Twig Extension for Plugin system
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class TwigExtension extends \Twig_Extension
{
    private $manager;

    public function __construct(PluginManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function getFilters()
    {
        return array(
            'area' => new \Twig_Filter_Method($this, 'displayArea', array('is_safe' => array('html'))),
        );
    }

    public function displayArea($name, $subject = null)
    {
        return $this->manager->displayArea($name, $subject);
    }

    public function getName()
    {
        return 'plugin_manager';
    }

}