<?php

namespace Bloggist\Component\Plugin;


/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
interface ParametersResolverInterface
{

    public function resolveParameters($object);

}