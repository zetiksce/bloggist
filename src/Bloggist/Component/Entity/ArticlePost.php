<?php

namespace Bloggist\Component\Entity;

/**
 * Bloggist\Component\Entity\ArticlePost
 */
class ArticlePost extends Post
{
    const TYPE = 'Article';

    /**
     * @var string $content
     */
    private $content;

    /**
     * Set content
     *
     * @param string $content
     * @return ArticlePost
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    public function getType()
    {
        return self::TYPE;
    }

}