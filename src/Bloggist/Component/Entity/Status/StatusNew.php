<?php

namespace Bloggist\Component\Entity\Status;

/**
 * Description of StatusNew
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class StatusNew extends Status
{
    const NAME = 'new';
    
    public function __construct()
    {
        parent::__construct(self::NAME);
    }
}