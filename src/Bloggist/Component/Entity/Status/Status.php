<?php

namespace Bloggist\Component\Entity\Status;

/**
 * Description of Status
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class Status implements StatusInterface
{
    private $name;
    
    public function __construct($name)
    {
        $this->name = $name;
    }
    
    public function __toString()
    {
        return $this->name;
    }
    
}