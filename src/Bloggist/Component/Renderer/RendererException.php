<?php

namespace Bloggist\Component\Renderer;

/**
 * Description of RendererException
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class RendererException extends \RuntimeException
{
    
}