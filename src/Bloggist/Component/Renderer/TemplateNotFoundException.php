<?php

namespace Bloggist\Component\Renderer;

/**
 * Description of TemplateNotFoundException
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
class TemplateNotFoundException extends RendererException
{

}