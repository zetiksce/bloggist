<?php

namespace Bloggist\Component\Renderer;

/**
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
interface RendererInterface
{

    public function render($object);

}