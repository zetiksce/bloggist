<?php

namespace Bloggist\Component\Rss;

use Suin\RSSWriter\Feed;

/**
 * Description of FeedBuilderInterface
 *
 * @author Zbigniew Czapran <zczapran@gmail.com>
 */
interface FeedBuilderInterface
{
    
    /**
     * @return Feed
     */
    public function build();

}